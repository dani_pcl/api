//parte común
var express = require('express')//referencia a la libreria express
var bodyParser = require('body-parser');//referencia a la librería body-parser
var app = express()

var requestJson = require('request-json')

app.use(bodyParser.json())//A la App le dices que lea el body con formato json

var urlMlabRaiz = "https://api.mlab.com/api/1/databases/bdbancodpc/collections"
var apiKey = "apiKey=nCJAZCUTg9xqTnC5C-xesRivvqMX6jrm"
var clienteMlab

var port = process.env.PORT || 3000 //defino una variable con el puerto

app.listen(port) //Levantar la aplicacion en el puerto definido

console.log("API escuchando en el puerto " + port)

// Obtener todos los datos de la base de datos de Usuarios
// A esta API sólo debería tener acceso el Admin
app.get('/apitechu/v1/usuarios', function(req, res)
{
  var clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios?" + apiKey)
  clienteMlab.get ('', function(err, resM, body){
    if (!err){
      res.send(body)
    }
  })
})

//Obtiene nombre y apellidos de un cliente identificado por el id
//En principio, no la usaremos
app.get('/apitechu/v1/usuarios/:id', function(req, res){
  var id = req.params.id
  var query = "q={'id':" + id + "}"

  var clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios?" + query + "&l=1&" + apiKey)

  clienteMlab.get ('', function(err, resM, body){
    if (!err){
      if (body.length>0){//Comprobar que devuelve informacion
        res.send({"nombre":body[0].first_name,"apellidos":body[0].last_name})
      }else {
        res.status(404).send('Usuario no encontrado')//Recurso no encontrado
      }
    }
  })
})

//Validar login si coincide el email y el password y marcarlo como logged
app.post('/apitechu/v1/login', function(req, res)
{
  var email = req.headers.email
  var password = req.headers.password
  var query= 'q={ $and: [ { "email": "' + email + '" }, { "password": "' + password + '" } ] }'

  var clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios?" + query + "&l=1&" + apiKey)

  clienteMlab.get ('', function(err, resM, body){

    if (!err){
      if (body.length==1){
          clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios")
          var cambio = '{"$set":{"logged":true}}'

          clienteMlab.put('?q={"id":' + body[0].id + '}&' + apiKey, JSON.parse(cambio),function(errP, resP, bodyP){
            if (!errP){
              res.send({"Operacion":"Login","Resultado":"OK", "Detalle":"","id":body[0].id,"nombre":body[0].first_name,"apellidos":body[0].last_name})
            }else{
              res.send({"Operacion":"Login","Resultado":"KO", "Detalle":"Error actualizar logged","id":body[0].id,"nombre":body[0].first_name,"apellidos":body[0].last_name})
            }
          })

      }else {
          res.status(404).send({"Operacion":"Login","Resultado":"KO", "Detalle":"Email y/o contraseña incorrectos"})
      }
    }
  })
})

//Validar login si coincide el email y el password y marcarlo como logged
app.post('/apitechu/v1/logout', function(req, res)
{
  var id = req.headers.id
  var query= 'q={ "id": ' + id + ', "logged":true }'
  var url_completa = urlMlabRaiz + "/usuarios?" + query + "&l=1&" + apiKey

  var clienteMlab = requestJson.createClient(url_completa)

  clienteMlab.get ('', function(err, resM, body){
    if (!err){
      if (body.length==1){//Comprobar que devuelve informacion
          var cambio = '{"$set":{"logged":false}}'

          clienteMlab.put('', JSON.parse(cambio),function(errP, resP, bodyP){
            if (!errP){
              res.send({"Operacion":"Logout","Resultado":"OK", "Detalle":"","id":body[0].id})
            }else{
              res.send({"Operacion":"Logout","Resultado":"KO", "Detalle":"Error actualizar logged","id":body[0].id})
            }
          })

      }else {
          res.status(404).send({"Operacion":"Logout","Resultado":"KO", "Detalle":"ID incorrecto o no logado"})
      }
    }
  })
})

//Dado un id de cliente devuelve todos los ibanes y sus saldos de sus cuentas
app.get('/apitechu/v1/cuentas', function(req, res){
  var idcliente = req.headers.idcliente
  var query = 'q={"idcliente":' + idcliente + '}&f={"iban":1,"saldo":1,"_id":0}'

  var clienteMlab = requestJson.createClient(urlMlabRaiz + "/cuentas?" + query + "&" + apiKey)

  clienteMlab.get ('', function(err, resM, body){
    if (!err){
      res.send(body)
    }else{
      res.status(404).send({"Operacion":"Consulta de cuentas","Resultado":"KO", "Detalle":"ID no encontrado"})
    }
  })
})

//Dado un iban de cuenta devolver sus movimientos
//En principio, no la usaremos
app.get('/apitechu/v1/usuario/cuenta/movimientos', function(req, res){
  var iban = req.headers.iban
  var query = 'q={"iban":' + iban + '}&f={"movimientos":1,"_id":0}'

  var clienteMlab = requestJson.createClient(urlMlabRaiz + "/cuentas?" + query + "&" + apiKey)

  clienteMlab.get ('', function(err, resM, body){
    if (!err){
      res.send(body)
    }else{
      res.status(404).send({"Operacion":"Consulta de movimientos","Resultado":"KO", "Detalle":"IBAN no encontrado"})
    }
  })
})

//Dado un id de cuenta devolver los movimientos con la marca de si son o no financiables
// Son financiables las operaciones de cargo de más de 300 euros que no han sido financiadas anteriormente
app.get('/apitechu/v1/usuario/cuenta/movimientos/financiables', function(req, res){
  var iban = req.headers.iban
  var query = 'q={"iban":' + iban + '}&f={"movimientos":1,"_id":0}'

  var clienteMlab = requestJson.createClient(urlMlabRaiz + "/cuentas?" + query + "&" + apiKey)

  clienteMlab.get ('', function(err, resM, body){
    if (!err){
      for (var i = 0; i < body[0].movimientos.length; i++) {
        if ((body[0].movimientos[i].importe < -300) && (!body[0].movimientos[i].financiada))  {
          body[0].movimientos[i].financiable = true
        }else {
          body[0].movimientos[i].financiable = false
        }
      }
      res.send(body)
    }else{
      res.status(404).send({"Operacion":"Consulta de movimientos","Resultado":"KO", "Detalle":"IBAN no encontrado"})
    }
  })
})


/*Dado un Iban y un ID de movimiento:
  Financiar operación.
  --------------------
   Para ello tenemos que:
   - Marcar operación como financiada
   - Insertar nueva operación con el abono en la cuenta
   - Incrementar saldo de la cuenta con el importe de la operacion
*/
app.post('/apitechu/v1/usuario/cuenta/movimientos/financiar', function(req, res){
  var iban = req.headers.iban
  var idmovimiento = req.headers.idmovimiento
  var fecha = new Date()
  var movimientonuevo =  {}
  var indice_operacion = 0

  var query = 'q={"iban":' + iban + '}'
  var url_completa = urlMlabRaiz + "/cuentas?" + query + "&l=1&" + apiKey

  var clienteMlab = requestJson.createClient(url_completa)

  clienteMlab.get ('', function(err, resM, body){
    if (!err){
      for (var i = 0; i < body[0].movimientos.length; i++) {
        if (body[0].movimientos[i].id == idmovimiento)  {
          body[0].movimientos[i].financiada = true
          indice_operacion = i
        }
      }
      movimientonuevo.id = i + 1
      movimientonuevo.fecha = (fecha.getFullYear() + "/" + (fecha.getMonth() +1) + "/" + fecha.getDate())
      movimientonuevo.importe = body[0].movimientos[indice_operacion].importe * (-1)
      movimientonuevo.moneda = body[0].movimientos[indice_operacion].moneda

      objeto_movimientos = body[0].movimientos
      objeto_movimientos[i] = movimientonuevo
      nuevo_saldo = body[0].saldo + movimientonuevo.importe

      var cambio = '{"$set":{"movimientos":' + JSON.stringify(objeto_movimientos) + '}}'

      clienteMlab.put('', JSON.parse(cambio),function(errP, resP, bodyP){
        if (!errP){
          var cambio = '{"$set":{"saldo":' + nuevo_saldo + '}}'

          clienteMlab.put('', JSON.parse(cambio),function(errP, resP, bodyP){
            if (!errP){
                res.send({"operacion":"Actualizacion saldo","Resultado":"OK"})
            }else {
                res.send({"operacion":"Actualizacion saldo","Resultado":"KO"})
            }
          })
        }else{
          res.send({"operacion":"Insercion financiacion","Resultado":"KO"})
        }
      })
    }else{
      res.status(404).send({"operacion":"Recuperar operacion","Resultado":"KO"})
    }
  })
})

app.post('/apitechu/v1/altausuarios', function(req, res)
{
  var first_name = req.headers.first_name
  var last_name = req.headers.last_name
  var email = req.headers.email
  var gender = req.headers.gender
  var ip_address = req.headers.ip_address
  var country = req.headers.country
  var password = req.headers.password

  var query= 'q={ "email": "' + email + '" }'
  var nuevo_usuario = {}
  var clientes = 0
  var clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios?" + apiKey)

  clienteMlab.get ('', function(err, resM, body){
    if (!err){
      clientes = body.length
      clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios?" + query + "&l=1&" + apiKey)

      clienteMlab.get ('', function(err, resM, body){
        if (!err){
          if (body.length==1){
            res.send({"Operacion":"Alta usuarios","Resultado":"KO", "Detalle":"Email ya existente"})
          }else {
            nuevo_usuario.id = clientes + 1
            nuevo_usuario.first_name = first_name
            nuevo_usuario.last_name = last_name
            nuevo_usuario.email = email
            nuevo_usuario.gender = gender
            nuevo_usuario.ip_address = ip_address
            nuevo_usuario.country = country
            nuevo_usuario.password = password
            clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios?" + apiKey)

            clienteMlab.post('', nuevo_usuario,function(errP, resP, bodyP){
              if (!errP){
                res.send({"Operacion":"Alta usuarios","Resultado":"OK", "Detalle":"Cliente dado de alta"})
              }else{
                res.send({"Operacion":"Alta usuarios","Resultado":"KO", "Detalle":"Error al insertar"})
              }
            })
          }
        }else {
          res.status(409).send({"Operacion":"Alta usuarios","Resultado":"KO", "Detalle":"Error al acceder con email"})
        }
      })
    }else{
      res.status(409).send({"Operacion":"Alta usuarios","Resultado":"KO", "Detalle":"Error al acceder"})
    }
  })
})
