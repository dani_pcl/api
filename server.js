//parte común
var express = require('express')//referencia a la libreria express
var bodyParser = require('body-parser');//referencia a la librería body-parser
var app = express()

var requestJson = require('request-json')

app.use(bodyParser.json())//A la App le dices que lea el body con formato json

var urlMlabRaiz = "https://api.mlab.com/api/1/databases/bdbancodpc/collections"
var apiKey = "apiKey=nCJAZCUTg9xqTnC5C-xesRivvqMX6jrm"
var clienteMlab

var port = process.env.PORT || 3000 //defino una variable con el puerto
var fs = require('fs')//referencia a la libreria fs que es para ficeros

app.listen(port) //Levantar la aplicacion en el puerto definido

console.log("API escuchando en el puerto " + port)

//parte de nuestra API
//var usuarios = require('./usuarios.json')
var usuarios = require('./password.json')
var cuentas = require('./cuentas.json')

console.log("Hola Mundo")

app.get('/apitechu/v1', function(req, res)
{
//  console.log(req)
  res.send({"mensaje":"Bienvenido a mi API"})
})

app.get('/apitechu/v1/usuarios', function(req, res)
{
  res.send(usuarios)
})

//Insertar registro en el fichero de memoria
app.post('/apitechu/v1/usuarios', function(req, res)
{
  var nuevo = {"first_name":req.headers.first_name,
               "country":req.headers.country}
  usuarios.push(nuevo)
  console.log(req.headers)
  const datos = JSON.stringify(usuarios)

//escribir datos en fichero
  fs.writeFile("./usuarios.json", datos, "utf8", function(err)
  {if (err)
    console.log(err)
   console.log("Fichero guardado")
  })
  res.send("Alta OK")
})

//borrar el registro -1 que recibamos en la URL
app.delete('/apitechu/v1/usuarios/:elegido', function(req, res){
  usuarios.splice(req.params.elegido-1, 1)
  res.send("Usuario borrado")
})

//demostracion de lo que coge de cada forma
app.post('/apitechu/v1/monstruo/:p1/:p2', function(req,res)
{
  console.log("Parametros")
  console.log(req.params)
  console.log("Query Strings")
  console.log(req.query)
  console.log("Headers")
  console.log(req.headers)
  console.log("Body")
  console.log(req.body)
})

//Insertar registro en el fichero de memoria
app.post('/apitechu/v2/usuarios', function(req, res)
{
/*  var nuevo = {"first_name":req.headers.first_name,
               "country":req.headers.country}
*/
  var nuevo = req.body

  usuarios.push(nuevo)
//  console.log(req.headers)
  const datos = JSON.stringify(usuarios)

//escribir datos en fichero
  fs.writeFile("./usuarios.json", datos, "utf8", function(err)
  {if (err)
    console.log(err)
   console.log("Fichero guardado")
  })
  res.send("Alta OK")
})

//Validar login si coincide el email y el password y marcarlo como logged
app.post('/apitechu/v2/usuarios/login', function(req, res)
{
  var email = req.headers.email
  var password = req.headers.password
  var idusuario = 0

  for (var i = 0; i < usuarios.length; i++) {
    if(usuarios[i].email == email && usuarios[i].password == password)
    {
      idusuario = usuarios[i].id
      usuarios[i].logged = true
      break;
    }
  }
  if (i == usuarios.length)
  {
    res.send({"encontrado":"no"})
  }else {
    res.send({"encontrado":"sí","id":idusuario})
  }
})

//Validar login si coincide el email y el password y marcarlo como logged
app.post('/apitechu/v2/usuarios/logout', function(req, res)
{
  var idusuario = req.headers.idusuario
  var encontrado = false
  var logado = false

  for (var i = 0; i < usuarios.length; i++) {
    if(usuarios[i].id == idusuario)
    {
      encontrado = true
      if (usuarios[i].logged)
      {
        logado = true
        usuarios[i].logged = false
      }
      break;
    }
  }
  if (encontrado){
    if (logado){
      res.send({"encontrado":"sí","id":idusuario,"logout":"OK"})
    }else {
      res.send({"encontrado":"sí","id":idusuario,"logout":"KO"})
    }
}else {
  res.send({"encontrado":"no","id":idusuario})
}
})

//APP DE CUENTAS
//Devuelve el fichero completo de cuentas
app.get('/apitechu/v1/cuentastotal', function(req, res)
{
  res.send(cuentas)
})

//Devuelve los ibanes que hay en el fichero completo de cuentas
app.get('/apitechu/v1/cuentas', function(req, res)
{
  var listadoibanes = []
  for (var i = 0; i < cuentas.length; i++) {
    listadoibanes[i] = cuentas[i].iban
  }
  res.send(listadoibanes)
})

//Devuelve los movimientos de una cuenta
app.get('/apitechu/v1/cuentas/movimientosiban', function(req, res)
{
  var iban = req.headers.iban
  var listadomovimientos = []

  for (var i = 0; i < cuentas.length; i++) {
    if (iban == cuentas[i].iban) {
      res.send(cuentas[i].movimientos)
      break
    }
  }
  res.send("Cuenta no encontrada")
})

//Devuelve las cuentas asociadas a un cliente
app.get('/apitechu/v1/cuentas/cuentascliente', function(req, res)
{
  var cliente = req.headers.idcliente
  var listadoibanes = []
  var j = 0

  for (var i = 0; i < cuentas.length; i++) {
    if (cliente == cuentas[i].idcliente) {
      listadoibanes[j] = cuentas[i].iban
      j++
    }
  }
  if (j == 0) {
    res.send("Cliente sin cuentas")
  }else {
    res.send(listadoibanes)
  }
})

//Devuelve las cuentas (y sus movimientos) asociadas a un cliente
app.get('/apitechu/v1/cuentas/cuentasymovimientoscliente', function(req, res)
{
  var cliente = req.headers.idcliente
  var listadoibanesArray = []
  var j = 0

   for (var i = 0; i < cuentas.length; i++) {
    if (cliente == cuentas[i].idcliente) {

      var listadoibanes = new Object()
        listadoibanes.iban = ""
        listadoibanes.movimientos = []

      listadoibanes.iban = cuentas[i].iban
      listadoibanes.movimientos = cuentas[i].movimientos

      listadoibanesArray.push(listadoibanes)

      j++
    }
  }

  if (j == 0) {
    res.send("Cliente sin cuentas")
  }else {
    res.send(listadoibanesArray)
  }
})

app.get('/apitechu/v5/usuarios', function(req, res)
{
  var clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios?" + apiKey)
  clienteMlab.get ('', function(err, resM, body){
    if (!err){
      res.send(body)
    }
  })
})

app.get('/apitechu/v5/usuarios/:id', function(req, res){
  var id = req.params.id
  var query = "q={'id':" + id + "}"
//con la f indico que campos quiero
//  var query = 'q={"id":' + id + '}&f={"first_name":1,"last_name":1,"_id"=0}'
  console.log("Query: ", query)
  var clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios?" + query + "&l=1&" + apiKey)
  console.log(urlMlabRaiz + "/usuarios?" + query + "&l=1&" + apiKey)
  clienteMlab.get ('', function(err, resM, body){
    if (!err){
      if (body.length>0){//Comprobar que devuelve informacion
        res.send({"nombre":body[0].first_name,"apellidos":body[0].last_name})
      }else {
        res.status(404).send('Usuario no encontrado')//Recurso no encontrado
      }
    }
  })
})

//Validar login si coincide el email y el password y marcarlo como logged
app.post('/apitechu/v5/login', function(req, res)
{
  var email = req.headers.email
  var password = req.headers.password
  var query= 'q={ $and: [ { "email": "' + email + '" }, { "password": "' + password + '" } ] }'

  var clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios?" + query + "&l=1&" + apiKey)

  clienteMlab.get ('', function(err, resM, body){

    if (!err){
      if (body.length==1){//Comprobar que devuelve informacion
          clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios")
          var cambio = '{"$set":{"logged":true}}'
          clienteMlab.put('?q={"id":' + body[0].id + '}&' + apiKey, JSON.parse(cambio),function(errP, resP, bodyP){
          if (!errP){
              res.send({"Operacion":"Login","Resultado":"OK", "Detalle":"","id":body[0].id,"nombre":body[0].first_name,"apellidos":body[0].last_name})
          }else{
              res.send({"Operacion":"Login","Resultado":"KO", "Detalle":"","id":body[0].id,"nombre":body[0].first_name,"apellidos":body[0].last_name})
          }})
      }else {
          res.status(404).send({"Operacion":"Login","Resultado":"KO", "Detalle":"Email y/o contraseña incorrectos"})//Recurso no encontrado
//      res.status(404).send("Operacion")
        }
      }
    })
})

//Validar login si coincide el email y el password y marcarlo como logged
app.post('/apitechu/v5/logout', function(req, res)
{
  var id = req.headers.id
  var query= 'q={ "id": ' + id + ', "logged":true }'
  var url_completa = urlMlabRaiz + "/usuarios?" + query + "&l=1&" + apiKey

  var clienteMlab = requestJson.createClient(url_completa)

  clienteMlab.get ('', function(err, resM, body){

    if (!err){
      if (body.length==1){//Comprobar que devuelve informacion
          var cambio = '{"$set":{"logged":false}}'

          clienteMlab.put('', JSON.parse(cambio),function(errP, resP, bodyP){
          if (!errP){
              res.send({"encontrado":"sí","logout":"si","id":body[0].id})
          }else{
              res.send({"encontrado":"sí","logout":"no","id":body[0].id})
          }})
      }else {
          res.status(200).send({"encontrado":"no"})
        }
      }
    })
})

//dado un id de cliente devolver la lista de ibanes
app.get('/apitechu/v5/cuentas', function(req, res){
  var idcliente = req.headers.idcliente
  var query = 'q={"idcliente":' + idcliente + '}&f={"iban":1,"saldo":1,"_id":0}'

  var clienteMlab = requestJson.createClient(urlMlabRaiz + "/cuentas?" + query + "&" + apiKey)

  clienteMlab.get ('', function(err, resM, body){
    if (!err){
      res.send(body)
      }
  })
})


//dado un iban de cuenta devolver los movimientos
app.get('/apitechu/v5/usuario/cuenta/movimientos', function(req, res){
  var iban = req.headers.iban
  var query = 'q={"iban":' + iban + '}&f={"movimientos":1,"_id":0}'

  console.log("Llaman a obtener movimientos con iban: ", iban);

  var clienteMlab = requestJson.createClient(urlMlabRaiz + "/cuentas?" + query + "&" + apiKey)

  clienteMlab.get ('', function(err, resM, body){
    if (!err){
      res.send(body)
      }
  })
})

//dado un id de cuenta devolver los movimientos con la marca de si son o no financiables
// son financiables las operaciones de cargo de más de 300 euros que no han sido financiadas ya
app.get('/apitechu/v5/usuario/cuenta/movimientos/financiables', function(req, res){
  var iban = req.headers.iban
  var query = 'q={"iban":' + iban + '}&f={"movimientos":1,"_id":0}'

  var clienteMlab = requestJson.createClient(urlMlabRaiz + "/cuentas?" + query + "&" + apiKey)

  console.log("Ruta:")
  console.log(urlMlabRaiz + "/cuentas?" + query + "&" + apiKey)

  clienteMlab.get ('', function(err, resM, body){

    console.log("body:",body)

    if (!err){
      console.log("Longitud body: ", body.length)
      for (var i = 0; i < body[0].movimientos.length; i++) {
        if ((body[0].movimientos[i].importe < -300) && (!body[0].movimientos[i].financiada))  {
          console.log("Caso 1")
          body[0].movimientos[i].financiable = true
        }else {
          console.log("Caso 2")
          body[0].movimientos[i].financiable = false
        }
      }
      res.send(body)
    }
  })
})


/*Financiar operación.
  --------------------
   Para ello tenemos que:
   - Marcar operación como financiada
   - Insertar nueva operación con el abono en la cuenta
   - Incrementar saldo de la cuenta con el importe de la operacion

*/

app.post('/apitechu/v5/usuario/cuenta/movimientos/financiar', function(req, res)
{
  var iban = req.headers.iban
  var idmovimiento = req.headers.idmovimiento

  console.log("/movimientos/financiar con iban = ", iban, " y idmovimiento = ", idmovimiento)

  var fecha = new Date()
//      clienteMlab.put('', JSON.stringify(reg_cuenta),function(errP, resP, bodyP){
  var movimientonuevo =  {}

  var indice_operacion = 0

  var query = 'q={"iban":' + iban + '}'
  var url_completa = urlMlabRaiz + "/cuentas?" + query + "&l=1&" + apiKey

  console.log("url_completa: ", url_completa)

  var clienteMlab = requestJson.createClient(url_completa)

  clienteMlab.get ('', function(err, resM, body){

  if (!err){
      console.log("Encontrado")
      for (var i = 0; i < body[0].movimientos.length; i++) {
        if (body[0].movimientos[i].id == idmovimiento)  {
          body[0].movimientos[i].financiada = true
          indice_operacion = i
        }
      }
      console.log("i:", i)
      console.log("indice_operacion:", indice_operacion)
      movimientonuevo.id = i + 1
      movimientonuevo.fecha = (fecha.getFullYear() + "/" + (fecha.getMonth() +1) + "/" + fecha.getDate())
      movimientonuevo.importe = body[0].movimientos[indice_operacion].importe * (-1)
      movimientonuevo.moneda = body[0].movimientos[indice_operacion].moneda
      console.log("movimientonuevo:", movimientonuevo)
      objeto_movimientos = body[0].movimientos
      console.log("objeto_movimientos 1:", objeto_movimientos)
      objeto_movimientos[i] = movimientonuevo
      console.log("objeto_movimientos 2:", objeto_movimientos)

      nuevo_saldo = body[0].saldo + movimientonuevo.importe
      console.log("body[0].saldo: ", body[0].saldo)
      console.log("movimientonuevo.importe: ", movimientonuevo.importe)
      console.log("nuevo_saldo:", nuevo_saldo)

      var cambio = '{"$set":{"movimientos":' + JSON.stringify(objeto_movimientos) + '}}'

      console.log("cambio movimientos:", cambio)
      clienteMlab.put('', JSON.parse(cambio),function(errP, resP, bodyP){
        if (!errP){
          console.log("bodyP de movimientos: ", bodyP)
          var cambio = '{"$set":{"saldo":' + nuevo_saldo + '}}'
          console.log("cambio saldo:", cambio)
          clienteMlab.put('', JSON.parse(cambio),function(errP, resP, bodyP){
            if (!errP){
                res.send({"operacion":"Actualizacion saldo","Resultado":"OK"})
            }else {
                res.send({"operacion":"Actualizacion saldo","Resultado":"KO"})
            }
          })
        }else{
          res.send({"operacion":"Insercion financiacion","Resultado":"KO"})
        }
      })
    }else{
      res.send({"operacion":"Recuperar operacion","Resultado":"KO"})
    }
  })
})

app.post('/apitechu/v5/altausuarios', function(req, res)
{
  var first_name = req.headers.first_name
  var last_name = req.headers.last_name
  var email = req.headers.email
  var gender = req.headers.gender
  var ip_address = req.headers.ip_address
  var country = req.headers.country
  var password = req.headers.password

  var query= 'q={ "email": "' + email + '" }'
  var nuevo_usuario = {}
  var clientes = 0
  var clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios?" + apiKey)

  console.log("ALTA DE USUARIOS")
  
  clienteMlab.get ('', function(err, resM, body){
    if (!err){

      console.log("body.length: ", body.length)
      clientes = body.length

      console.log("-" + urlMlabRaiz + "/usuarios?" + query + "&l=1&" + apiKey + "-");

      var clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios?" + query + "&l=1&" + apiKey)

      clienteMlab.get ('', function(err, resM, body){

        console.log("body tras select: ",body)
        console.log("body.length: ", body.length)

        if (!err){
          if (body.length==1){//Comprobar que devuelve informacion
            res.send({"Operacion":"Alta usuarios","Resultado":"KO", "Detalle":"Email ya existente"})
          }else {
            console.log("Paso1")
            nuevo_usuario.id = clientes + 1
            nuevo_usuario.first_name = first_name
            nuevo_usuario.last_name = last_name
            nuevo_usuario.email = email
            nuevo_usuario.gender = gender
            nuevo_usuario.ip_address = ip_address
            nuevo_usuario.country = country
            nuevo_usuario.password = password
            console.log("Paso2")
            clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios?" + apiKey)

//        cambio = '{"usuario":' + JSON.stringify(nuevo_usuario) + '}'
            console.log("nuevo_usuario: ", nuevo_usuario)
//angel        var prueba = JSON.stringify(nuevo_usuario)
//angel        console.log("prueba: ", prueba);
            console.log("clienteMlab: ", clienteMlab)
//angel        clienteMlab.post('', JSON.parse(prueba),function(errP, resP, bodyP){
            clienteMlab.post('', nuevo_usuario,function(errP, resP, bodyP){
            console.log("bodyP: ", bodyP)
              if (!errP){
                res.send({"Operacion":"Alta usuarios","Resultado":"OK", "Detalle":"Cliente dado de alta"})
              }else{
                res.send({"Operacion":"Alta usuarios","Resultado":"KO", "Detalle":"Error al insertar"})
              }
            })
          }
        }else {
          console.log({"Operacion":"Alta usuarios","Resultado":"KO", "Detalle":"Error al acceder"})
        }
      })
    }
  })
})

app.post('/apitechu/v5/altausuariosauxiliar', function(req, res)
{
  var email = req.headers.email
  var password = req.headers.password
  var query= 'q={ "email": "' + email + '" }'
  var nuevo_usuario = {}
  var clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios?" + query + "&l=1&" + apiKey)

  clienteMlab.get ('', function(err, resM, body){

    if (!err){
      if (body.length==1){//Comprobar que devuelve informacion
          res.send({"Operacion":"Alta usuarios","Resultado":"KO", "Detalle":"Email ya existente"})
      }else {
        console.log("Paso1")
        nuevo_usuario.id = 1502
        nuevo_usuario.first_name = "Perico"
        nuevo_usuario.last_name = "De los Palotes"
        nuevo_usuario.email = "perico@palotes.es"
        nuevo_usuario.gender = "Male"
        nuevo_usuario.ip_address = "192.192.192.192"
        nuevo_usuario.country = "Spain"
        nuevo_usuario.password = "AAAAAAA"
        console.log("Paso2")
        clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios?" + apiKey)

        cambio = JSON.stringify('{"id": "1502"},{"first_name":"Perico"},{"last_name":"De los Palotes"},{"email":"perico@palotes.es"},{"gender":"Male"},{"ip_adress":"192.192.192.192"},{"Country":"Spain"},{"password":"AAAAAAAA"}')
        console.log("cambio: ", cambio)
        clienteMlab.post('',cambio,function(errP, resP, bodyP){
        console.log("bodyP: ", bodyP)
        if (!errP){
            res.send({"encontrado":"sí","logado":"si","id":body[0].id,"nombre":body[0].first_name,"apellidos":body[0].last_name})
        }else{
            res.send({"encontrado":"sí","logado":"no","id":body[0].id,"nombre":body[0].first_name,"apellidos":body[0].last_name})
        }})
      }
    }else {
        console.log({"Operacion":"Alta usuarios","Resultado":"KO", "Detalle":"Email ya existente"})
    }
    })
})
